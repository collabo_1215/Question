
import java.io.*;
import java.util.*;
/**
 * 
 * @author Maruyama
 *
 */
public class TestMain {

	public static void main(String[] args) {
		String filename = args[0];
		String line;
		//問題を作るArrayListと問題と答えを紐づけるHashMap
		ArrayList<String> questions = new ArrayList<String>();
		Map<String, String> map = new HashMap<String, String>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			//読み込んだ行がnullでないなら
			while ((line = reader.readLine()) != null) {
				//その行をquestionに追加する
				String	question = line;
				questions.add(line);
				//次の行をanswerに追加する
				String	answer = reader.readLine();
				//questionとanswerを対応させる
				map.put(question, answer);
			}
			
		} catch (FileNotFoundException e) {
			System.out.println(filename + "が見つかりません。");
			
		} catch (IOException e) {
			System.out.println(e);
		}
		Player1 p1 = new Player1();
		//numにMAXQUESTIONの数を入れる
		int num = Quiz.quiz(questions, map, p1);
		
		//正解率を出す
		double rate = (p1.trueAnswer /num) * 100;
		System.out.println("---------------------------------");
		System.out.println("正解率は" + rate + "％です。");
		System.out.println("");
		System.out.println("間違えた問題は");
		//間違えた問題をすべて出すfor
		for (int i = 1; i < p1.falseQuestion.size(); i++) {
			System.out.println(i + " : " + p1.falseQuestion.get(i) + " = " + map.get(p1.falseQuestion.get(i)));
		}
		System.out.println("");
	}

}
