import java.util.*;
import java.io.*;
/**
 * 
 * @author Maruyama
 *
 */
public class Quiz implements QuizInterface {
	//
	public static int quiz(ArrayList<String> questions, Map<String, String> map, Player1 p1) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line;
		
		System.out.println("問題は全部で" + MAX_QUESTION + "問です。");
		//MAXQUESTIONの回数分問題を作る
		for (int i = 0; i < MAX_QUESTION; i++) {
			int n = (int)(Math.random() * questions.size() - 1) + 1;
			System.out.println("---------------------------------");
			System.out.println("第" + (i + 1) + "問");
			System.out.println(questions.get(n));
			while (true) {
				try {
					//入力した文字に（）をつけて返す
					line = "（" + reader.readLine() + "）";
					//if文へ飛ぶ
					break;
				} catch (IOException e) {
					
				} catch (NullPointerException e) {
					
				}
			}
			//正解かどうか確かめるif文
			if (line.equals(map.get(questions.get(n)))) {
				System.out.println(line);
				System.out.println("正解！");
				System.out.println(map.get(questions.get(n)));
				//プレイヤーの正解数を増やす
				p1.trueAnswer++;
				
			} else {
				System.out.println(line);
				System.out.println("不正解");
				System.out.println(" ");
				System.out.println("正解は " + map.get(questions.get(n)) + " です");
				//プレイヤーの間違い数を増やす
				p1.falseAnswer++;
				//間違えた問題を保存しておくadd
				p1.falseQuestion.add(questions.get(n));
			}
			
		}
		//quiz(questions, map, p1)にMAXQUESTIONを返す
		return MAX_QUESTION;
		
	}
}
