package audioSounds_2;

import javax.swing.JFrame;

public class SoundBack1 extends JFrame{
	public SoundBack1() {
		setTitle("漢字検定");  //タイトル
		setSize(Main.SCREEN_WIDTH,Main.SCREEN_HEIHT);// 画面サイズ
		setResizable(false); //一回作られた画面はいじられない
		setLocationRelativeTo(null);  //実行時画面が中央になるように
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //ゲームの窓口を閉じるとゲームのクローズできるように
		setVisible(true);  //作ったプログラムが通常にできるように				
	}
}
