
import java.util.ArrayList;
/**
 * 
 * @author Maruyama
 *
 */
public class Player1 {
	//プレイヤーの正解数、間違い数を定めるフィールド
	int trueAnswer;
	int falseAnswer;
	//間違えた問題を順番に入れていくArrayList
	ArrayList<String> falseQuestion = new ArrayList<String>();
	
	Player1() {
		//開始する際正解、間違いを0に、間違えた問題をnullにする
		trueAnswer = 0;
		falseAnswer = 0;
		falseQuestion.add(null);
	}

}
