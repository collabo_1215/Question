package audioSounds;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
//import javax.swing.JButton;
import javax.swing.JFrame;

//　GUIベースのために　extends JFrameを使用
public class SoundBack1 extends JFrame {
	
	// 画面の出力
	private Image screenImage;
	private Graphics screenGraphic;
	
//	private ImageIcon startButtonEnteredImage = new ImageIcon(getClass().getResource("/image/startButtonEntered.png"));
//	private ImageIcon startButtonBasicImage = new ImageIcon(getClass().getResource("/image/startButtonBasic.png"));
//	private ImageIcon quitButtonEnteredImage = new ImageIcon(getClass().getResource("/image/quitButtonEntered.png"));
//	private ImageIcon quitButtonBasicImage = new ImageIcon(getClass().getResource("/image/quitButtonBasic.png"));
//
//	private JButton startButton = new JButton(startButtonBasicImage);
//	private JButton quitButton = new JButton(quitButtonBasicImage);

	private Image introBackground = new ImageIcon(getClass().getResource("/images/main.jpg")).getImage();
	
	public SoundBack1() {
		setTitle("漢字検定");  //タイトル
		setSize(Main.SCREEN_WIDTH,Main.SCREEN_HEIHT);// 画面サイズ
		setResizable(false); //一回作られた画面はいじられない
		setLocationRelativeTo(null);  //実行時画面が中央になるように
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //ゲームの窓口を閉じるとゲームのクローズできるように
		setVisible(true);  //作ったプログラムが通常にできるように		
		
		
//		introBackground = new ImageIcon(Main.class.getResource("../images/main.jpg")).getImage();

//		startButton.setBounds(1245, 0, 450, 100);
//		startButton.setBorderPainted(false);
//		startButton.setContentAreaFilled(false);
//		startButton.setFocusPainted(false);
//		startButton.addMouseListener(new MouseAdapter() {
//
//			@Override
//			public void mouseEntered(MouseEvent e) {
//				startButton.setIcon(startButtonEnteredImage);
//			}
//
//			@Override
//			public void mouseExited(MouseEvent e) {
//
//				startButton.setIcon(startButtonBasicImage);
//
//			}
//
//			@Override
//			public void mousePressed(MouseEvent e) {
//				System.exit(0);
//			}
//			});
//
//		add(startButton);
//		
//		quitButton.setBounds(1245, 0, 450, 100);
//		quitButton.setBorderPainted(false);
//		quitButton.setContentAreaFilled(false);
//		quitButton.setFocusPainted(false);
//		quitButton.addMouseListener(new MouseAdapter() {
//
//			@Override
//			public void mouseEntered(MouseEvent e) {
//				quitButton.setIcon(quitButtonEnteredImage);
//			}
//
//			@Override
//			public void mouseExited(MouseEvent e) {
//				quitButton.setIcon(quitButtonBasicImage);
//			}
//
//			@Override
//			public void mousePressed(MouseEvent e) {
//				System.exit(0);
//
//			}
//			});
//
//		add(quitButton);
	
		// 音楽（Musicクラス）の再生
		Music introMusic = new Music("source.mp3", true);
		introMusic.start();
	}
	
	//　ダブルバッファリングを行う。  これは描画に時間がかかる場合に、その途中経緯が見えてしまうのを防ぐ目的で利用します。
	public void paint(Graphics g) {
		screenImage = createImage(Main.SCREEN_WIDTH, Main.SCREEN_HEIHT); //1280*720のイメージを作り　/　バッファイメージに描画処理1
		screenGraphic = screenImage.getGraphics(); //screenImageをGraphic化させる　/　バッファイメージに描画処理2
		screenDraw(screenGraphic); //screenDrawメソッドを使って画面に引き込む　/　バッファイメージに描画処理3
		g.drawImage(screenImage, 0, 0, null);  // gにバッファイメージを描画
	}

	public void screenDraw(Graphics g) {
		g.drawImage(introBackground, 0, 0, null);
		// paintComponents(g);
		this.repaint();
	}

	public void actionPerformed(ActionEvent e) {
		// TODO 自動生成されたメソッド・スタブ
		g.drawImage(introBackground, 0, 0, null); //screenImageを全体イメージに　0,0位置に入れ込み
		// paintComponents(g);
		this.repaint(); //paintメソッドに戻る
	}
}
	
