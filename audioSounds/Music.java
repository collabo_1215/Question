package audioSounds;

// ｍｐ3のファイル使うのには　JLayer1.0.1　ダウンロードが必要です。

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import javazoom.jl.player.Player;

public class Music extends Thread {

	private Player player; //Java Zoom中のJLayer
	private boolean isLoop; //音楽の無限ループ設定
	private File file;
	private FileInputStream fis;
	private BufferedInputStream bis;

	public Music(String name, Boolean isLoop) {
		try {
			this.isLoop = isLoop; 
			file = new File(getClass().getResource("/Music/" + name).toURI());
			this.isLoop = isLoop;
			file = new File(getClass().getResource("/Music/" + name).toURI());//fileの読み込み 位置
			fis = new FileInputStream(file);
			bis = new BufferedInputStream(fis); // Bufferedに入れて読み込み
			player = new Player(bis);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public int getTime() { // 現在に流れている音楽の位置を教えて、　時間分析をしてくれる。
		if (player == null)
			return 0;
		return player.getPosition();
	}

	public void close() { // 安定的な音楽の終了
		isLoop = false;
		player.close();
		this.interrupt(); // threadを停止させる。
	
	}

	@Override
	public void run() {
		try {
			do {
				player.play();
				fis = new FileInputStream(file);
				bis = new BufferedInputStream(fis);
				player = new Player(bis);
			} while (isLoop);  //isLoopが true==無限ループ
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
